import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Hcp from './Hcp';
import Hcf from './Hcf';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  handleHeaderMenuClick = (uri) => {
    this.props.history.push(uri);
  }

  render() {
    return (
      <div className="App container">
        <Router>
          <div className="col-sm-12">
            <ul className="list-group list-group-horizontal">
              <li className="list-group-item"><Link to="/">Home</Link></li>
              <li className="list-group-item"><Link to="/hcp">Hcp</Link></li>
            </ul>
          </div>
          <Route exact path="/" component={Hcf} />
          <Route path="/hcp" component={Hcp} />
        </Router>
      </div>
    );
  }
}

export default App;