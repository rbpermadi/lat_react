import React from "react";

const HcpTable = ({ hcps, selectHcp }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Speciality</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      {hcps.map(item => (
        <tr className={(typeof item.selected === 'undefined' || item.selected === false) ? "" : "table-green"} key={item.id}>
          <td>
            <div>
              <img src={"http://localhost:3031/"+item.image_path}></img>
            </div>
            <div>{item.name}</div>
          </td>
          <td className="col-sm-6">{item.speciality.name}</td>
          <td className="col-sm-3"><button type="button" onClick={() => selectHcp(item.id)}>{(typeof item.selected === 'undefined' || item.selected === false) ? "Booking" : "Unbooking"}</button></td>
        </tr>
      ))}
      </tbody>
    </table>
  );
};

export default HcpTable;
