import React from "react";

const Modal = ({ handleClose, show, children, id }) => {
  const showHideClassName = show ? "modal d-block" : "modal d-none";

  return (
    <div className={showHideClassName}>
      <div className="modal-container">
        <h1>{id}</h1>
        {children}
        <button type="button" className="modal-close" onClick={handleClose}>
          close
        </button>
      </div>
    </div>
  );
};

export default Modal;
