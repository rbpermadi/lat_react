import React from 'react';
import HcpTable from './components/HcpTable';
import Modal from "./components/Modal";
import 'bootstrap/dist/css/bootstrap.min.css';

class Hcp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
      hcpId: "",
      name: "",
      modalInputName: "",
      hcps: []
    };
  }

  componentDidMount() {
    fetch('/api/v1/hcps')
    .then(response => response.json())
    .then(result => {
      this.setState({
        hcps: result.data
      });
    })
    .catch(e => {
        console.log(e);
    });
  }

  handleChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    this.setState({
      [name]: value
    });
  }

  handleSubmit = (e) => {
    this.setState({ name: this.state.modalInputName });
    this.modalClose();
  }

  modalOpen = (i) => {
    console.log(i)
    this.setState({ modal: true });
  }

  selectHcp = (i) => {
    const hcps = this.state.hcps.map(item => {
      if(item.id === parseInt(i)){
        if (typeof item.selected === 'undefined' || item.selected === false) {
          item.selected = true
        } else {
          item.selected = false
        }
      }
      return item;
    });

    this.setState({ hcps: hcps });
  }

  modalClose = () => {
    this.setState({
      modalInputName: "",
      modal: false
    });
  }

  render() {
    return (
      <div className="Hcp">
        <h1>Hcps</h1>
        <button type="button" onClick={()=>{this.modalOpen(1)}}>open modal</button>
        <HcpTable hcps={this.state.hcps} selectHcp={this.selectHcp}></HcpTable>
        <Modal show={this.state.modal} handleClose={this.modalClose} id={this.state.hcpId}>
          <h2>Hello Modal</h2>
          <div className="form-group">
            <label>Enter Name:</label>
            <input
              type="text"
              hcpid="123"
              value={this.state.modalInputName}
              name="modalInputName"
              onChange={this.handleChange}
              className="form-control"
            />
          </div>
          <div className="form-group">
            <button onClick={this.handleSubmit} type="button">
              Save
            </button>
          </div>
        </Modal>
      </div>
    );
  }
}

export default Hcp;